package com.carlos.project.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class Startup
{
	private static final Logger log = LoggerFactory.getLogger(Startup.class);

	@Bean
	public Docket docs()
	{
		return new Docket(DocumentationType.SWAGGER_2)
				   .select()
				   .apis(RequestHandlerSelectors.any())
				   .paths(PathSelectors.any())
				   .build();
	}

	public static void main(String[] args)
	{
		SpringApplication.run(Startup.class, args);
		log.info("Application ran as expected and started properly");
	}
}

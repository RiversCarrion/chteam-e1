package com.carlos.project.service.beans;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name="C_USER")
@JsonIgnoreProperties({"hibernateLazyInitializer","handler"})
public class User
{
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name="ID_USER")
    private Long id;

    @Column(name="NAME_USER")
    private String name;

    @Column(name="LAST_NAME_USER")
    private String lastName;

    @Column(name="USERNAME_USER")
    private String userName;

    @Column(name="PASSWORD_USER")
    @JsonIgnore
    private String password;

    @Column(name="BIRTHDAY_USER")
    private Date birthday;

    @Column(name="PHONE_USER")
    private int phone;

    @Column(name="EMAIL_USER")
    private String email;

    @ManyToOne(cascade = {CascadeType.ALL})
    @JoinColumn(name="COUNTRY_USER")
    private Country country;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }
}

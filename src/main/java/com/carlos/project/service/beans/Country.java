package com.carlos.project.service.beans;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name="COUNTRY")
public class Country implements Serializable
{
    @GeneratedValue
    @Id
    @Column(name="ID_COUNTRY", updatable = false)
    private Long id;

    @Column(name="NAME_COUNTRY", updatable = false)
    private String countryName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}

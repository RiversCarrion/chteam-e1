package com.carlos.project.service.data;

import com.carlos.project.service.beans.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long>
{
}

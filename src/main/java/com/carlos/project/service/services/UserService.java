package com.carlos.project.service.services;

import com.carlos.project.service.beans.User;
import com.carlos.project.service.data.UserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService
{
    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserRepository repository;

    public List<User> getAllUsers()
    {
        log.info("Retrieving all Users");
        return repository.findAll();
    }

    public User getUser(Long id)
    {
        log.debug("Getting user with id {}", id);
        return repository.getOne(id);
    }

    public User addUser(User user)
    {
        log.debug("Creating the newer user {}", user.getUserName());
        return repository.save(user);
    }
}

package com.carlos.project.service.dto;

import java.io.Serializable;

public class CountryDTO implements Serializable
{
    private Long id;
    private String countryName;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}

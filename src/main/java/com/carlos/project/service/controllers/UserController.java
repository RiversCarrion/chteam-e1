package com.carlos.project.service.controllers;

import com.carlos.project.service.beans.User;
import com.carlos.project.service.dto.UserDTO;
import com.carlos.project.service.services.UserService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/users")
public class UserController
{
    @Autowired
    private UserService service;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public List<UserDTO> findUsers()
    {
        List<User> users = service.getAllUsers();
        return users.stream().map(user -> convertToDTO(user)).collect(Collectors.toList());
    }

    @GetMapping(path="/{id-user}")
    @ResponseStatus(HttpStatus.OK)
    public User findUser(@PathVariable("id-user") Long id)
    {
        return service.getUser(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public UserDTO createUser(@RequestBody UserDTO body)
    {
        User user = convertToEntity(body);
        User createdUser = service.addUser(user);

        return convertToDTO(createdUser);
    }

    private UserDTO convertToDTO(User user)
    {
        return modelMapper.map(user, UserDTO.class);
    }

    private User convertToEntity(UserDTO userDTO)
    {
        return modelMapper.map(userDTO, User.class);
    }
}

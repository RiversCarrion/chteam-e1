FROM openjdk:8u131-jdk-alpine

EXPOSE 8080

WORKDIR /usr/local/bin/

COPY maven/chteam-0.0.1-SNAPSHOT.jar chteam.jar

CMD ["java", "-jar", "chteam.jar"]